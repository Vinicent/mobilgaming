using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

/*
public class KevinCode : MonoBehaviour
{
   
                   // week 2 Slide 69
                   if (Input.touchCount > 0)​
                   {​
                       Touch touch = Input.GetTouch(0);​
                       horizontal = touch.position.x / Screen.width > 0.5f ? 1f : -1f;​
                   }
   
                   // week 2 Slide 71
                   _rigidbody.velocity = new Vector3(Input.GetAxis("Horizontal") * _playerModel.MovementForce * Time.deltaTime, 0, 0f);

                   _rigidbody.velocity = new Vector3(horizontal * _playerModel.MovementForce * Time.deltaTime, 0, 0f);​
   
                   // week 2 Slide 72 (Final Code)
                   private void FixedUpdate()​
                   {​
                #if UNITY_ANDROID && !UNITY_EDITOR​
                         float horizontal = 0;​
                         if (Input.touchCount > 0)​
                         {​
                            Touch touch = Input.GetTouch(0);​
                            horizontal = touch.position.x / Screen.width > 0.5f ? 1f : -1f;​
                         }​
                         ​
                         _rigidbody.velocity = new Vector3(horizontal * _playerModel.MovementForce * Time.deltaTime, 0, 0f);​
                #else​
                       _rigidbody.velocity = new Vector3(Input.GetAxis("Horizontal") * _playerModel.MovementForce * Time.deltaTime, 0, 0f);​
                #endif​
                   }
   
//FERTIG 16.06.2021   


               // Week 3 Slide 46 CloudBuilds
               public class MyBuildScript
               {
                  public static void JenkinsBuildGame()
                  {
                     //Do your thing!
                     string[] scenes = new[] {"MainScene.unity"};
                     string locationPathName = "../Builds/Game.exe";
                     BuildTarget standaloneWindows64 = BuildTarget.StandaloneWindows64;
                     BuildOptions buildOptions = BuildOptions.Development | BuildOptions.AllowDebugging;
                     UnityEditor.BuildPipeline.BuildPlayer(scenes, locationPathName, standaloneWindows64, buildOptions);
                  }
               }
   
//^Wir benutzten Cloudsaves


               //W05 / Folie 40
               PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder().Build();​
               PlayGamesPlatform.InitializeInstance(config);​
               PlayGamesPlatform.Activate();
   
               //W 05 / Folie 41
            // authenticate user:​
               PlayGamesPlatform.Instance.Authenticate(SignInInteractivity.CanPromptOnce,​
               (result) =>​
               {​

                  // do something upon authentication (e.g. Error handling or enabling   functionality)​
               });


   
                //W05 / Folie 43
                if (!_socialPlatformActivated)​
                {​
                PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder().Build();​
                PlayGamesPlatform.InitializeInstance(config);​
                PlayGamesPlatform.Activate();​
                _socialPlatformActivated = true;​
                ​
                // authenticate user:​
                PlayGamesPlatform.Instance.Authenticate(SignInInteractivity.CanPromptOnce,​
                (result) =>​
                {​
                   _currentState = LoadState;​
                   _currentState.StateEnter();​
                });​
                }​
                else​
                {​
                _currentState = LoadState;​
                _currentState.StateEnter();​
                }

//Eingefügt aber noch nicht Fertig GameFlow Skript Zeile 65

            //Weeek 5 Folie 58 Leaderboards
            Social.ReportScore(ScoreManager.Instance.HighScore,
            GPGSIds.leaderboard_global,   
            (res) =>   
            {      
            // handle score post result if desired   
            });

            //Week 5 Folie 58
            PlayGamesPlatform.Instance.ShowLeaderboardUI();

            //Week 5 Folie 60
            PlayGamesPlatform.Instance.LoadScores(
            GPGSIds.leaderboard_global,
            LeaderboardStart.PlayerCentered,
            10,
            LeaderboardCollection.Public,
            LeaderboardTimeSpan.AllTime,
            FillWithData
            );
// angefangen aber unsicher GameOverState.cs

// Week 06 Slide 18
// unlock achievement (achievement ID "Cfjewijawiu_QA")​
Social.ReportProgress("Cfjewijawiu_QA", 100.0f, (success) => ​
{​
// handle success or failure​
});​

// Week 06 Slide 19

PlayGamesPlatform.Instance.IncrementAchievement("Cfjewijawiu_QA", 5, ( success) => ​
{​
// handle success or failure​
});​

// Week 06 Slide 20  Open it by calling 

Social.ShowAchievmentsUI();

                    //Week 06 Slide 29 Saved Games
                    PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder()​
                    // enables saving game progress.​
                    .EnableSavedGames()​
                    .Build();​
                    PlayGamesPlatform.InitializeInstance(config);​

                    //Week 06 Slide 30

                    ISavedGameClient savedGameClient = PlayGamesPlatform.Instance.SavedGame;​
                    savedGameClient.ShowSelectSavedGameUI("Select saved game",​
                    maxNumToDisplay,​
                    allowCreateNew,​
                    allowDelete,​
                    OnSavedGameSelected);

                    //Week 06 Slide 31

                    void OpenSavedGame(string filename)​
                    {​
                    ISavedGameClient savedGameClient = PlayGamesPlatform.Instance.SavedGame;​
                    savedGameClient.OpenWithAutomaticConflictResolution(​
                       filename, ​
                       DataSource.ReadCacheOrNetwork, ​
                       ConflictResolutionStrategy.UseLongestPlaytime, ​
                       OnSavedGameOpened​
                       );​
                    }​
                    ​
                    public void OnSavedGameOpened(SavedGameRequestStatus status, ISavedGameMetadata game)​
                    {​
                    if (status == SavedGameRequestStatus.Success)​
                    {​
                       // handle reading or writing of saved game.​
                    } ​
                    else ​
                    {​
                       // handle error​
                    }​
                    }

                    //Week 06 Slide 32

                    void SaveGame (ISavedGameMetadata game, byte[] savedData, TimeSpan totalPlaytime) ​
                    {​
                    ISavedGameClient savedGameClient = PlayGamesPlatform.Instance.SavedGame;​
                    ​
                    SavedGameMetadataUpdate.Builder builder = new SavedGameMetadataUpdate.Builder();​
                    builder = builder​
                       .WithUpdatedPlayedTime(totalPlaytime)​
                    .WithUpdatedDescription("Saved game at " + DateTime.Now);​
                       ​
                    if (savedImage != null) ​
                    {​
                       // savedImage is an instance of Texture2D​
                       // call getScreenshot() to set savedImage​
                       byte[] pngData = savedImage.EncodeToPNG();​
                       builder = builder.WithUpdatedPngCoverImage(pngData);​
                    }​
                       ​
                    SavedGameMetadataUpdate updatedMetadata = builder.Build();​
                    savedGameClient.CommitUpdate(game, updatedMetadata, savedData, OnSavedGameWritten);​
                    }​

                    //Week 06 Slide 33

                    public void OnSavedGameWritten (SavedGameRequestStatus status, ISavedGameMetadata game) ​
                    {​
                    if (status == SavedGameRequestStatus.Success)​
                    {​
                       // handle reading or writing of saved game.​
                    }​
                    else ​
                    {​
                       // handle error​
                    }​
                    }​
                    ​
                    public Texture2D getScreenshot() ​
                    {​
                    // Create a 2D texture that is 1024x700 pixels from which the PNG will be​
                    // extracted​
                    Texture2D screenShot = new Texture2D(1024, 700);​
                    ​
                    // Takes the screenshot from top left hand corner of screen and maps to top​
                    // left hand corner of screenShot texture​
                    screenShot.ReadPixels(new Rect(0, 0, Screen.width, (Screen.width/1024)*700), 0, 0);​
                    return screenShot;​
                    }​

                    //Week 06 Slide 34

                    /// <summary>​
                    ///    write the save game as .json to disk.​
                    /// </summary>​
                    /// <param name="path">Path where the save game is being stored.</param>​
                    /// <param name="saveData">Actual save data</param>​
                    public void Serialize(string path, SaveData saveData)​
                    {​
                    string serializedJSON = JsonUtility.ToJson(saveData);​
                    File.WriteAllText(path, serializedJSON);​
                    }​

                    //Week 06 Slide 35

                    public void JsonToBinary()​
                    {          ​
                    string json = "{Score:10}";​
                    byte[] bytes = Encoding.ASCII.GetBytes(json);​
                    }​

                    public void BinaryToJson(byte[] bytes)​
                    {          ​
                    string str = Encoding.ASCII.GetString(bytes);​
                    }

                    //Week 06 Slide 37

                    void LoadGameData (ISavedGameMetadata game) ​
                    {​
                    ISavedGameClient savedGameClient = PlayGamesPlatform.Instance.SavedGame;​
                    savedGameClient.ReadBinaryData(game, OnSavedGameDataRead);​
                    }​
                    ​
                    public void OnSavedGameDataRead (SavedGameRequestStatus status, byte[] data) ​
                    {​
                    if (status == SavedGameRequestStatus.Success)​
                    {​
                       // handle processing the byte array data​
                    } ​
                    else ​
                    {​
                       // handle error​
                    }​
                    }​

                    //Week 06 Slide 38

                    void DeleteGameData (string filename) ​
                    {​
                    // Open the file to get the metadata.​
                    ISavedGameClient savedGameClient = PlayGamesPlatform.Instance.SavedGame;​
                    savedGameClient.OpenWithAutomaticConflictResolution(​
                       filename, ​
                       DataSource.ReadCacheOrNetwork,​
                       ConflictResolutionStrategy.UseLongestPlaytime, ​
                       DeleteSavedGame);​
                    }​
                    ​
                    public void DeleteSavedGame(SavedGameRequestStatus status, ISavedGameMetadata game) ​
                    {​
                    if (status == SavedGameRequestStatus.Success)​
                    {​
                       ISavedGameClient savedGameClient = PlayGamesPlatform.Instance.SavedGame;​
                       savedGameClient.Delete(game);​
                    } ​
                    else ​
                    {​
                       // handle error​
                    }​
                    }​


                        //Skipped because Saved games doesnt work for us

                    */
