using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GooglePlayGames
{
    public class Counter : MonoBehaviour
    {
        public float timer = 60f;
        // Update is called once per frame
        void Update()
        {
            timer -= Time.deltaTime;

            if (timer <= 0)
            {
                Debug.Log("Master Dodger achievment");
                // unlock achievment Master dodger
                Social.ReportProgress("CgkI_cuEzKYdEAIQAg", 100.0f, (succes) =>
                    {
                        // handle success or failure
                    });
            }
        }
    }
}

