using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GooglePlayGames
{
    public class Timer : MonoBehaviour
    {
        public float counter;
        

        // Update is called once per frame
        void Update()
        {
            counter += Time.deltaTime;

            if (counter >= 60)
            { 
                Debug.Log("Master Dodger achievment");
                // unlock achievement Master dodger
                Social.ReportProgress("CgkI_cuEzKYdEAIQAg", 100.0f,(success) =>
                {
                    // handle success or failure​
                });
            }
        }
    }
}