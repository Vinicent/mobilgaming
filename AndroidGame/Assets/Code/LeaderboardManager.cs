using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using GooglePlayGames.BasicApi;
using GooglePlayGames.BasicApi.SavedGame;
using GooglePlayGames;
using ScoreSystem;
using Core;
using TMPro;
using UnityEngine.UI;

public class LeaderboardManager : Singleton<LeaderboardManager> {


	// Update is called once per frame
	public void LeaderboardFillerGlobal()
    {
		//Weeek 5 Folie 58 Leaderboards
		Social.ReportScore(ScoreManager.Instance.HighScore,
		GPGSIds.leaderboard_best_dodger,
		(res) =>
		{
				// handle score post result if desired   
			});

		//Week 5 Folie 58
		PlayGamesPlatform.Instance.ShowLeaderboardUI();


		//Week 5 Folie 60
		PlayGamesPlatform.Instance.LoadScores(
		GPGSIds.leaderboard_best_dodger,
		LeaderboardStart.PlayerCentered,
		10,
		LeaderboardCollection.Public,
		LeaderboardTimeSpan.AllTime,
		FillWithData
		);
	}

	public void LeaderboardFillerSocial()
	{
		//Weeek 5 Folie 58 Leaderboards
		Social.ReportScore(ScoreManager.Instance.HighScore,
		GPGSIds.leaderboard_best_dodger,
		(res) =>
		{
			// handle score post result if desired   
		});

		//Week 5 Folie 58
		PlayGamesPlatform.Instance.ShowLeaderboardUI();


		//Week 5 Folie 60
		PlayGamesPlatform.Instance.LoadScores(
		GPGSIds.leaderboard_best_dodger,
		LeaderboardStart.PlayerCentered,
		10,
		LeaderboardCollection.Social,
		LeaderboardTimeSpan.AllTime,
		FillWithData
		);
	}
	//Name, Rank and Score of our Players
	public TextMeshProUGUI[] playerName;
    
	public TextMeshProUGUI[] score;
    
	public TextMeshProUGUI[] rank;
	
	// fills our Leaderboard with Values
	private void FillWithData(LeaderboardScoreData fillData)
    {
		if(fillData == null)
        {
			return;
        }
		for (int i = 0; i < 10; i++)
        {
			if(fillData.Scores == null)
            {
				break;
            }
			playerName[i].text = fillData.Scores[i].leaderboardID;
			score[i].text = fillData.Scores[i].value.ToString();
			rank[i].text = fillData.Scores[i].rank.ToString();			
		}
		
		
	
}
}
