using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;
using CurrencySystem;
using UnityEngine.UI;

//Week 7 Slide 40 Ads
    public class MyAdHandler : MonoBehaviour, IUnityAdsListener
    {
        public string MyPlacementId = "PressDaButton";
        private const string GameId = "4207017";
        private const bool IsTestMode = true;

        public Button AdButton;

    private void Start()
        {
            Advertisement.AddListener(this);
            Advertisement.Initialize(GameId, IsTestMode);

            AdButton.onClick.AddListener(ShowRewardedAd);
    }

        private void OnDestroy()
        {
            Advertisement.RemoveListener(this);
        }

        public void ShowRewardedAd()
        {
            Debug.Log("Ad wird abgespielt");
            Advertisement.Show(MyPlacementId);
        }

        public void OnUnityAdsReady(string placementId)
        {
            // enable button
        }

        public void OnUnityAdsDidError(string message)
        {
            // log the message
        }

        public void OnUnityAdsDidStart(string placementId)
        {
        }

        public void OnUnityAdsDidFinish(string placementId, ShowResult showResult)
        {
            if (!placementId.Equals(MyPlacementId))
            {
                return;
            }
    
            if (showResult == ShowResult.Finished)
            {
                Reward();
            }
        }
        private void Reward()
        {
        // here we reward the player for watching the ad with 50 coins
             CoinPurse.Instance.AddCoinsToTotal(50);
        }

    }