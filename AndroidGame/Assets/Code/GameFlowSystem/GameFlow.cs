﻿using Core;
using GameFlowSystem.States;
using PlayerSystem;
using UnityEngine;
using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using GooglePlayGames.BasicApi;
using GooglePlayGames.BasicApi.SavedGame;
using GooglePlayGames;
using UnityEngine.UI;

namespace GameFlowSystem
{
	/// <summary>
	/// 	State Machine controlling the game flow. Implements state pattern.
	/// </summary>
	public class GameFlow : Singleton<GameFlow>
	{
		#region Serialize Fields

		[SerializeField] private MainMenuState _mainMenuState;
		[SerializeField] private PauseGameState _pauseGameState;
		[SerializeField] private StartGameState _startGameState;
		[SerializeField] private PlayGameState _playGameState;
		[SerializeField] private GameOverState _gameOverState;
		[SerializeField] private SaveState _saveState;
		[SerializeField] private LoadState _loadState;
		[SerializeField] private bool _socialPlatformActivated;


		public Button signInButton;
		public Button signOutButton;
		#endregion

		#region Private Fields

		private IGameState _currentState;
		private PlayerController _player;

		#endregion

		#region Properties

		public GameOverState GameOverState => _gameOverState;
		public bool IsGameRunning => _currentState.Equals(PlayGameState);
		public LoadState LoadState => _loadState;
		public MainMenuState MainMenuState => _mainMenuState;
		public PauseGameState PauseGameState => _pauseGameState;
		public PlayerController Player => _player;
		public PlayGameState PlayGameState => _playGameState;
		public SaveState SaveState => _saveState;
		public StartGameState StartGameState => _startGameState;

		#endregion

		#region Unity methods

		protected override void Awake()
		{
			base.Awake();

			_player = FindObjectOfType<PlayerController>();

			_currentState = LoadState;
			_currentState.StateEnter();

			Time.timeScale = 0f;

			signInButton.onClick.AddListener(SignIn);
			signOutButton.onClick.AddListener(SignOut);

			//W05 / Folie 43
			
			if (!_socialPlatformActivated)
			{
				PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder().Build();
				PlayGamesPlatform.InitializeInstance(config);
				PlayGamesPlatform.Activate();
				_socialPlatformActivated = true;

				// Authenticate the User Automatically
				SignIn();
			}
			else
			{
			_currentState = LoadState;
			_currentState.StateEnter();
			}
			
			
		}
		private void Update()
		{
			if (_currentState == null)
			{
				return;
			}

			// state pattern - update current state, hceck if it needs to be switched. Exit current one and enter new one if required.
			IGameState nextState = _currentState.StateUpdate();
			if (nextState != _currentState)
			{
				_currentState.StateExit();
				_currentState = nextState;
				_currentState.StateEnter();
			}
		}


		#endregion
		//Sign the User in when a button is pressed
		public void SignIn()
        {
			Debug.Log("User Signs in");
			// authenticate user:
			PlayGamesPlatform.Instance.Authenticate(SignInInteractivity.CanPromptOnce,
				(result) =>
				{
					_currentState = LoadState;
					_currentState.StateEnter();
				});
		}
		//Signs the user out if the button is Pressed
		public void SignOut()
        {
			Debug.Log("User is signed Out");
			PlayGamesPlatform.Instance.SignOut();
		}
	}
}